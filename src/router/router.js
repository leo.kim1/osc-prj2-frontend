import Vue from 'vue';
import VueRouter from 'vue-router';
import HttpResponseExample from "../components/HttpResponseExample";
import HttpResponseExample2 from "../components/HttpResponseExample2";
import InputBox from "../components/InputBox";

Vue.use(VueRouter)

const routes = [
  { path:"/", name: "root", component:HttpResponseExample },
  { path:"/input", name: "inputBox", component:InputBox },
  { path:"/home", name: "home", component:HttpResponseExample },
  { path:"/home2", name: "home2", component:HttpResponseExample2 },
];

const router = new VueRouter({
  mode: "history",
  routes,
});
export default router
